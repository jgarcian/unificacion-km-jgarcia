class CreateGroupLays < ActiveRecord::Migration[5.0]
  def change
    create_table :group_lays do |t|
      t.integer :group_id
      t.integer :orderNumber
      t.integer :fieldFormat_id

      t.timestamps
    end
  end
end
