class CreateGroupFilterDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :group_filter_details do |t|
      t.integer :group_id
      t.integer :conditionNumber
      t.string :connector
      t.integer :length
      t.integer :denied
      t.string :operator
      t.string :parenthesis
      t.integer :position
      t.string :value
      t.integer :fieldFormat_id
      t.string :key_id
      t.integer :lay_id
      t.integer :format_id

      t.timestamps
    end
  end
end
