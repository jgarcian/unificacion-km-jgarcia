class CreateGroupFilters < ActiveRecord::Migration[5.0]
  def change
    create_table :group_filters do |t|
      t.integer :group_id
      t.string :description
      t.string :backGroundColor
      t.string :letterColor
      t.integer :orderNumber
      t.integer :key_id

      t.timestamps
    end
  end
end
