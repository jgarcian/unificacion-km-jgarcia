class CreateFieldLays < ActiveRecord::Migration[5.0]
  def change
    create_table :field_lays do |t|
      t.integer :lay_id
      t.string :name
      t.integer :position, :limit => 2
      t.integer :length, :limit => 2
      t.integer :dataType, :limit => 2
      t.integer :level, :limit => 2
      t.integer :father, :limit => 1
      t.integer :replaced, :limit => 1
      t.string :alias
      t.integer :lengthField, :limit => 2
      t.integer :keyFieldLayId#, :limit => 2

      t.timestamps
    end
  end
end
