class CreateUserWorkSpaces < ActiveRecord::Migration[5.0]
  def change
    create_table :user_work_spaces do |t|
      t.integer :user_id
      t.integer :porcentual_session_id
      t.integer :area

      t.timestamps
    end
  end
end
