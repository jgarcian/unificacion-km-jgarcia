class AddNewValuesToPorcentualSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :porcentual_sessions, :inactive, :integer
    add_column :porcentual_sessions, :actualizar, :integer
  end
end
