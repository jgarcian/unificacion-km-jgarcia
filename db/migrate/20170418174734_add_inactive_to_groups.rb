class AddInactiveToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :inactive, :boolean
  end
end
