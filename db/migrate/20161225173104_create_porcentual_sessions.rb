class CreatePorcentualSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :porcentual_sessions do |t|
      t.belongs_to :format, index: true

      t.integer :lay_id
      #t.integer :format_id
      t.string  :description
      t.string  :sessionColor
      t.integer :interval
      t.integer :inactive
      t.integer :actualizar
      t.integer :key_id

      t.timestamps
    end
  end
end
