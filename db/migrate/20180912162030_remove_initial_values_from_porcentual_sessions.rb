class RemoveInitialValuesFromPorcentualSessions < ActiveRecord::Migration[5.0]
  def change
    remove_column :porcentual_sessions, :inactive, :bit
    remove_column :porcentual_sessions, :actualizar, :bit
  end
end
