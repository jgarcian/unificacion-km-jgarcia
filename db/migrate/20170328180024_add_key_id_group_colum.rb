class AddKeyIdGroupColum < ActiveRecord::Migration[5.0]
  def change
    add_column :group_columns, :key_id , :integer
    add_column :group_columns, :format_id, :integer
    add_column :group_columns, :lay_id , :integer
  end
end
