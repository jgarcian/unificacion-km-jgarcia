class AddFieldsToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :detail, :integer
    add_column :groups, :reference, :integer
    add_column :groups, :area, :integer
    add_column :groups, :hist_alert, :integer
    add_column :groups, :hist_trans, :integer
  end
end
