class CreateUserWorkGroupSpaces < ActiveRecord::Migration[5.0]
  def change
    create_table :user_work_group_spaces do |t|
      t.integer :group_id
      t.integer :groupColumn_id
      t.integer :typeChart
      t.integer :area

      t.timestamps
    end
  end
end
