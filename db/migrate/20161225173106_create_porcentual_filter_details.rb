class CreatePorcentualFilterDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :porcentual_filter_details do |t|

      t.belongs_to :porcentual_filter, index: true
      t.belongs_to :field_format, index: true

      #t.integer :porcentual_filter_id
      t.integer :conditionNumber
      t.string :connector
      t.integer :length
      t.integer :denied
      t.string  :operator
      t.string  :parenthesis
      t.integer :position
      t.string  :value
      #t.integer :field_format_id

      t.timestamps
    end
  end
end
