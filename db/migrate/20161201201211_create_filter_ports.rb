class CreateFilterPorts < ActiveRecord::Migration[5.0]
  def change
    create_table :filter_ports do |t|
      t.integer :format_id
      t.integer :lay_id
      t.integer :port
      t.string :description

      t.timestamps
    end
  end
end
