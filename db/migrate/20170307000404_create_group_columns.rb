class CreateGroupColumns < ActiveRecord::Migration[5.0]
  def change
    create_table :group_columns do |t|
      t.integer :group_id
      t.integer :column
      t.string :title
      t.integer :typeOperation
      t.integer :fieldFormat_id
      t.integer :typeChart

      t.timestamps
    end
  end
end
