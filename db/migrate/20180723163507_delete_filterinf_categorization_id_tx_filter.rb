class DeleteFilterinfCategorizationIdTxFilter < ActiveRecord::Migration[5.0]
  def change
    remove_column :tx_filters, :filtering_categorization_id
  end
end
