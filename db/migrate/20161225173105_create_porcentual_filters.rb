class CreatePorcentualFilters < ActiveRecord::Migration[5.0]
  def change
    create_table :porcentual_filters do |t|

      t.belongs_to :porcentual_session, index: true

      #t.integer :porcentual_session_id
      t.integer :group_id
      t.string :description
      t.string :backgroundColor
      t.string :letterColor
      t.integer :order_number
      t.integer :key_id

      t.timestamps
    end
  end
end
