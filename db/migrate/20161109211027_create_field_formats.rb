class CreateFieldFormats < ActiveRecord::Migration[5.0]
  def change
    create_table :field_formats do |t|
      t.integer :format_id
      t.integer :lay_id
      t.integer :consecutive, :limit => 2
      t.integer :fieldLay_id
      t.integer :position
      t.integer :length
      t.integer :field_type, :limit => 2
      t.integer :keyFieldFormatId#, :limit => 2


      t.timestamps
    end
  end
end
