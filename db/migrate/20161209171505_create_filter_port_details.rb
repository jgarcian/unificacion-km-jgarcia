class CreateFilterPortDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :filter_port_details do |t|
      t.integer :filterPort_id
      t.integer :conditionNumber
      t.string :connector
      t.integer :length
      t.integer :denied
      t.string :operator
      t.string :parenthesis
      t.integer :position
      t.string :value
      t.integer :fieldLay_id
      t.integer :key_id

      t.timestamps
    end
  end
end
