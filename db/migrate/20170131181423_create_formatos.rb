class CreateFormatos < ActiveRecord::Migration[5.0]
  def change
    create_table :formatos do |t|
      t.integer :lay_id
      t.string :description
      t.integer :keyFormatId

      t.timestamps
    end
  end
end
