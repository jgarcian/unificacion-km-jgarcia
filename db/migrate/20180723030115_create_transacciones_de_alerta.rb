class CreateTransaccionesDeAlerta < ActiveRecord::Migration[5.0]
  def change
    create_table :transacciones_de_alerta do |t|
      t.integer :idAlerta
      t.integer :idTran
      t.string :ttTableName
    end
  end
end
