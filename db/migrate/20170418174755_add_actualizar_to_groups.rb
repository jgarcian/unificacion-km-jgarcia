class AddActualizarToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :actualizar, :boolean
  end
end
