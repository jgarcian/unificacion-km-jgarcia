class CreateOperationTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :operation_types do |t|
      t.string :description
      t.integer :typeOperation

      t.timestamps
    end
  end
end
