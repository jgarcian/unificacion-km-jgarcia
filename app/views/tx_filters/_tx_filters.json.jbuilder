json.extract! tx_filters, :id, :created_at, :updated_at
json.url tx_filters_url(tx_filters, format: :json)
