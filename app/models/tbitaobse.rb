class Tbitaobse < ApplicationRecord
  belongs_to :tbitacora, foreign_key: :IdAlerta
  belongs_to :testadosxalerta, foreign_key: :IdEstado
  belongs_to :user, foreign_key: :IdUsuarioAtencion
end
