class TxFilter < ApplicationRecord
  belongs_to :tx_session
  has_many :tx_filter_details
end
