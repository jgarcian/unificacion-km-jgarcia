class Tgrupos < ApplicationRecord
  has_many :tbitacora, foreign_key: :IdGrupo
  has_many :tperfiles, foreign_key: :IdGrupo
end
