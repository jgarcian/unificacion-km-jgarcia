//= require morris/raphael-2.1.0.min.js
//= require morris/morris.js
//= require datapicker/bootstrap-datepicker.js

// Porcentual

$(document).ready(function () {
    $("#envValCom").click(function () {

        var finiParse = Date.parse($("#inicioCom").val())
        var ffinParse = Date.parse($("#finalCom").val())
        var fecha1 = $("#inicioCom").val()
        //fecha1 = new Date(fecha1)
        var fecha2 = $("#finalCom").val()
        //fecha2 = new Date(fecha2)
        var sesion = $("#sessionCom").val()
        if (($("#inicioCom").val() == "") || ($("#finalCom").val() == "")) {
            if (($("#inicioCom").val() == "")) {
                //$("#inicioCom").focus()
            }
            else if ($("#finalCom").val() == "") {
                //$("#finalCom").focus()
            }
        }
        else if (finiParse > ffinParse){
            $("#finalCom").val("")
            $("#finalCom").focus()
        }
        else  if ($("#sessionCom").val() == "") {
            $("#sessionCom").focus().select()
        }
        else {
            //$(this).attr("disabled", true)
            $('.area1').load('comparasions/dynamic?btn=btn1&fecha1='+fecha1 +'&fecha2='+fecha2+'&sess='+sesion+'&area=session1' )
            $("#inicioCom").val("")
            $("#finalCom").val("")
            $("#sessionCom").val("")
            $('#inicioCom').data('datepicker').remove();
            $('#finalCom').data('datepicker').remove();
            //$('.area1').load('home/dynamic')

        }
    })

    $("#envValCom2").click(function () {
        var finiParse = Date.parse($("#inicioCom").val())
        var ffinParse = Date.parse($("#finalCom").val())
        var fini2Parse = Date.parse($("#inicioCom2").val())
        var ffin2Parse = Date.parse($("#finalCom2").val())

        var fecha1 = $("#inicioCom").val()
        var fecha2 = $("#finalCom").val()
        var sesion = $("#sessionCom").val()

        var fecha12 = $("#inicioCom2").val()
        var fecha22 = $("#finalCom2").val()
        var sesion2 = $("#sessionCom2").val()


        if (($("#inicioCom").val() == "") || ($("#finalCom").val() == "") || ($("#sessionCom").val() == "")) {
            if (($("#inicioCom").val() == "")) {
                // $("#inicioCom").focus()
                //alert("FocIni1")
            }
            else if ($("#finalCom").val() == "") {
                // $("#finalCom").focus()
                //alert("FocFin1")
            }
            else if (finiParse > ffinParse) {
                //alert('I2: '+finiParse+' F2: '+ffinParse)
                $("#finalCom").val("")
                $("#finalCom").focus()
            }
            else if ($("#sessionCom").val() == "") {
                $("#sessionCom").focus().select()
            }
        }
        else if (finiParse > ffinParse) {
            //alert('I2: '+finiParse+' F2: '+ffinParse)
            $("#finalCom").val("")
            $("#finalCom").focus()
        }
        else if (($('#inicioCom2').val() == "") || ($('#finalCom2').val() == "") || ($("#sessionCom2").val() == "")) {


            if ($("#inicioCom2").val() == "") {
                // $("#inicioCom2").focus()
                //alert("FocIni2")
            }
            else if ($("#finalCom2").val() == "") {
                // $("#finalCom2").focus()
                //alert("FocFin2")
            }
            else if (fini2Parse > ffin2Parse) {
                $("#finalCom2").val("")
                $("#finalCom2").focus()
            }
            else if ($("#sessionCom2").val() == "") {
                $("#sessionCom2").focus().select()
            }
        }
        else if (fini2Parse > ffin2Parse) {
            $("#finalCom2").val("")
            $("#finalCom2").focus()
        }
        else{
            $('.area1').load('comparasions/dynamic?fecha1='+fecha1 +'&fecha2='+fecha2+'&sess='+sesion+'&area=session1', function(){
                $('.area2').load('comparasions/dynamic?btn=btn2&fecha1='+fecha12 +'&fecha2='+fecha22+'&sess='+sesion2+'&area=session2')
            })

            $("#inicioCom").val("")
            $("#finalCom").val("")
            $("#sessionCom").val("")
            $('#inicioCom').data('datepicker').remove();
            $('#finalCom').data('datepicker').remove();

            $("#inicioCom2").val("")
            $("#finalCom2").val("")
            $("#sessionCom2").val("")
            $('#inicioCom2').data('datepicker').remove();
            $('#finalCom2').data('datepicker').remove();



        }
    })

    $("#comparaCom").click(function () {
        if ($(this).prop("checked") == true) {
            $("#inicioCom2").attr("disabled", false)
            $("#finalCom2").attr("disabled", false)
            $("#sessionCom2").attr("disabled", false)
            $("#envValCom2").attr("disabled", false)
            $("#fecha12Com").show()
            $("#fecha22Com").show()
            $("#grafica2Comp").show()
            $("#envValCom").attr("disabled", true)
            $("#envValCom").hide()

            $('.area1').load('comparasions/empty', function(){
                $('.area2').load('comparasions/empty')
            })

        }
        else if ($(this).prop("checked") == false) {
            $("#inicioCom2").attr("disabled", true)
            $("#finalCom2").attr("disabled", true)
            $("#sessionCom2").attr("disabled", true)
            $("#envValCom2").attr("disabled", true)
            $("#inicioCom2").val("")
            $("#finalCom2").val("")
            $("#sessionCom2").val("")
            $("#fecha12Com").hide()
            $("#fecha22Com").hide()
            $("#grafica2Comp").hide()
            $("#envValCom").attr("disabled", false)
            $("#envValCom").show()

            $('.area1').load('comparasions/empty', function(){
                $('.area2').load('comparasions/empty')
            })

        }
    })


})