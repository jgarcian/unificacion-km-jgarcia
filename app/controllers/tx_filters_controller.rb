class TxFiltersController < ApplicationController
  before_action :set_tx_filters, only: [:show, :edit, :update, :destroy]

  # GET /tx_filters
  # GET /tx_filters.json
  def index
    @tx_filters = TxFilters.all
  end

  # GET /tx_filters/1
  # GET /tx_filters/1.json
  def show
  end

  # GET /tx_filters/new
  def new
    @tx_filter = TxFilters.new

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true
  end

  # GET /tx_filters/1/edit
  def edit

    # Gon para traes los campos del formato y layout correspondiente
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    ## Gon para llenar selects Layout, Formato de ventana modal

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true
  end

  # POST /tx_filters
  # POST /tx_filters.json
  def create

    @idSession = params[:idSession]
    @desFilter = params[:nameFilter]
    @colorFilter = params[:general]
    @typeFilter = params[:typeFilter]

    @tx_filter = TxFilter.new
    @tx_filter.Session_id = @idSession
    @tx_filter.Description = @desFilter
    @tx_filter.Backgroundcolor = @colorFilter
    @tx_filter.Order_number = @typeFilter
    @tx_filter.save


    #// Creacion del filtro
    @detalleFiltro = params[:filtro]
    @idUser = params[:keyId]

    @cont = 0
    @numCondi = 0
    @crearCondicion = false

    @conector
    @negado
    @operador
    @parentesis
    @valor
    @campoFormatoId
    @posision
    @length

    @detalleFiltro.each do | datoFiltro |
      @cont += 1

      if ( @cont === 1 )
        @conector = datoFiltro
      elsif ( @cont === 2 )
        @negado = datoFiltro
      elsif ( @cont === 3 )
        @operador = datoFiltro
      elsif ( @cont === 4 )
        @parentesis = datoFiltro
      elsif ( @cont === 5 )
        @valor = datoFiltro
      elsif ( @cont === 6 )
        @campoFormatoId = datoFiltro
      elsif ( @cont === 7 )
        @posision = datoFiltro
      elsif ( @cont === 8 )
        @length = datoFiltro
        @cont = 0
        @crearCondicion = true
      end

      if ( @crearCondicion  === true )
        @crearCondicion = false
        @numCondi += 1
        # @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params) marco error enviandolo desde el metodo new de session
        @tx_filter_detail = TxFilterDetail.new
        @tx_filter_detail.filter_id = @tx_filter.id
        @tx_filter_detail.conditionnumber = @numCondi
        @tx_filter_detail.connector = @conector
        @tx_filter_detail.denied = @negado
        @tx_filter_detail.operator = @operador
        @tx_filter_detail.parenthesis = @parentesis
        @tx_filter_detail.value = @valor
        @tx_filter_detail.fieldFormat_id = @campoFormatoId
        @tx_filter_detail.position = @posision
        @tx_filter_detail.lenght = @length
        @tx_filter_detail.key_id = @idUser
        @tx_filter_detail.save
      end
    end

    # @txfilter = TxFilters.new()
    # val1 = params[:Session_id]
    # val2 = params[:Description]
    # val3 = params[:Backgroundcolor]
    # val4 = params[:Order_number]
    # val5 = params[:Key_id]
    #
    # if( params[:_method].present? && params[:_method] == 'create' )
    #   @txfilter.Session_id = params[:Session_id]
    #   @txfilter.Description = params[:Description]
    #   @txfilter.Backgroundcolor = params[:Backgroundcolor]
    #   @txfilter.Order_number = params[:Order_number]
    #   @txfilter.Key_id = params[:Key_id]
    #   @txfilter.save
    #
    #   @tx_filter_detail = TxFilterDetail.where(:filter_id => nil).where(:key_id => current_user.id)
    #   @tx_filter_detail.each do | detalleFiltro|
    #     detalleFiltro.filter_id = @txfilter.id
    #     detalleFiltro.key_id = nil
    #     detalleFiltro.save
    #   end
    # end




    # respond_to do |format|
    #   if @tx_filter.save
    #     format.html { redirect_to @tx_filter, notice: 'Tx filter was successfully created.' }
    #     format.json { render :show, status: :created, location: @tx_filter }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @tx_filter.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /tx_filters/1
  # PATCH/PUT /tx_filters/1.json
  def update
    respond_to do |format|
      if @tx_filter.update(tx_filters_params)
        format.html { redirect_to action: :edit, notice: 'Tx filter was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_filter }
      else
        format.html { render :edit }
        format.json { render json: @tx_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_filters/1
  # DELETE /tx_filters/1.json
  def destroy
    @tx_filters.destroy
    respond_to do |format|
      format.html { redirect_to tx_filters_url, notice: 'Tx filter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filters
      @tx_filter = TxFilters.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filters_params
      params.require(:tx_filters).permit(:Id, :Session_id, :Group_id, :Description, :Backgroundcolor, :Lettercolor, :Order_number, :Key_id)
    end
end
