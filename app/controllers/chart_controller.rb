class ChartController < ApplicationController

  respond_to :html, :js

  def index
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 11 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Charts Index"


    # @tbit = Tbitacora.new
    # @tbit.IdUsuario = @id_usuario
    # @tbit.IdAplicacion = @id_aplicacion
    # @tbit.IdTipoReg = @idTipoReg #Index
    # @tbit.Fecha = @fecha
    # @tbit.Hora  = @hora
    # @tbit.Mensaje = @men
    # @tbit.save

    @hora = Time.now
    @tiempo = @hora.strftime('%Y-%m-%d')

    ######################### Dynamic

    usuario = User.find(current_user.id)
    @usuario = usuario.name
    @holaDynamic = "HolaDynamic"


    @hora = Time.now
    @tiempo = @hora.strftime('%Y-%m-%d')

    menos = Time.now - 2.hours
    horita = Time.now

    sql = "
select ct.id, contador_categorias.Fecha, contador_categorias.Hora , suma from
(select cf.id_categori, contador.Fecha, contador.Hora , sum(cantidad) suma from
(select idFiltro, Fecha, substring(Hora,1,5) Hora, count(*) cantidad from tbitacora where IdTipoReg = 3 and Fecha = '#{@tiempo}' and Hora between '#{menos.strftime("%H:%M")}' and '#{horita.strftime("%H:%M")}' group by idFiltro, Fecha, Hora) as contador
left join categori_filters cf on contador.IdFiltro = cf.id_filter
group by cf.id_categori, contador.Fecha, contador.Hora) as contador_categorias
join filtering_categorizations ct on contador_categorias.id_categori = ct.id
order by Fecha, Hora, ct.id"

    @datos = ActiveRecord::Base.connection.exec_query(sql)
    @datos1 = ActiveRecord::Base.connection.exec_query(sql)


    if @datos1.present?
      puts 'No hay Datos'
    else
      puts 'Si hay datps'
    end

    registro = ""
    hora = ""
    hora2 = ""
    count = 0
    keys = {}
    color = {}
    datosScript = Array.new
    total = 0
    hash = {}
    colorHash = {}
    count = 0
    @tx_filter = FilteringCategorization.all
    @tx_filter.each do |txfil|
      count+=1
      hash.store(txfil.id, txfil.name_categorization)
      colorr = Colors.obtenerColor(count)
      colorHash.store(txfil.id, colorr)
    end

    clores = colorHash


    @datos.each do |datos|
      idFil = datos['id']
      hora = datos['Hora']
      valores = datos['suma']

      comp = hash[idFil]
      label = hash[idFil].to_s.downcase.tr(" ", "_")

      if hora != hora2.to_s
        if registro.to_s.length > 0
          total = count
          datosScript.push(("{"+registro+"},").gsub("", "").html_safe).to_s
        end
        count = 0
        hora2 = hora
        registro = "time: '"+hora2+"'"
      end
      count += 1

      if total <= count
        keys.store(count, label)
        color.store(count, colorHash[idFil])
      end

      registro += ",#{label}: #{valores}"
    end

    if registro.to_s.length > 0
      datosScript.push(("{#{registro}}").gsub("", "").html_safe).to_s
    end

    keysScript = Array.new
    colorScript = Array.new
    n = 0
    keys.each do |k|
      nu = n+=1
      if nu < keys.length
        keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
      elsif nu = keys
        keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
      end
    end

    d = 0
    color.each do |c|
      nu = d+=1
      if nu < color.length
        colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
      elsif nu = color
        colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
      end
    end

    keysScriptD = Array.new
    colorScriptD = Array.new
    n = 0
    keys.each do |k|
      nu = n+=1
      if nu < keys.length
        keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
      elsif nu = keys
        keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
      end
    end

    d = 0
    color.each do |c|
      nu = d+=1
      if nu < color.length
        colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
      elsif nu = color
        colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
      end
    end

    @label = keysScript
    @color = colorScript

    @labelD = keysScriptD
    @colorD = colorScriptD
    @datosScript = datosScript

    # respond_to do |f|
    #   f.html {render partial: 'dynamic'}
    # end
    ######################### Dynamic



    ######################### Pie

    sql = "select ct.id , suma from
(select cf.id_categori , sum(cantidad) suma from
(select idFiltro, count(*) cantidad from tbitacora where IdTipoReg = 3 group by idFiltro) as contador
left join categori_filters cf on contador.IdFiltro = cf.id_filter
group by cf.id_categori) as contador_categorias
join filtering_categorizations ct on contador_categorias.id_categori = ct.id
order by ct.id"

    hash = {}
    chash = {}
    keys = {}
    registro = ""

    datosScript = Array.new
    @datos = ActiveRecord::Base.connection.exec_query(sql)

    fil_cat = FilteringCategorization.all
    contador = 0
    fil_cat.each do |fi_ca|
      contador += 1
      hash.store(fi_ca.id,fi_ca.name_categorization)
      chash.store(fi_ca.id,Colors.obtenerColor(contador))
    end


    @datos.each do |da|
      idCar = da['id']
      valor = da['suma'].to_s.downcase.tr(" ", "_")
      label = hash[idCar].to_s.downcase.tr(" ", "_")
      color = chash[idCar].to_s.downcase.tr(" ", "_")
      registro = "label: '#{label}', data: #{valor}, color: '#{color}'"

      if registro.to_s.length
        datosScript.push(("{#{registro} },").gsub(" ", " ").html_safe).to_s
      end

      #registro = "label: '#{label}', data: #{valor}"

    end

    @valores = datosScript

    ######################### Pie


  end

  def dynamic
    usuario = User.find(current_user.id)
    @usuario = usuario.name
    @holaDynamic = "HolaDynamic"


    @hora = Time.now
    @tiempo = @hora.strftime('%Y-%m-%d')

    menos = Time.now - 2.hours
    horita = Time.now

    sql = "
select ct.id, contador_categorias.Fecha, contador_categorias.Hora , suma from
(select cf.id_categori, contador.Fecha, contador.Hora , sum(cantidad) suma from
(select idFiltro, Fecha, substring(Hora,1,5) Hora, count(*) cantidad from tbitacora where IdTipoReg = 3 and Fecha = '#{@tiempo}' and Hora between '#{menos.strftime("%H:%M")}' and '#{horita.strftime("%H:%M")}' group by idFiltro, Fecha, Hora) as contador
left join categori_filters cf on contador.IdFiltro = cf.id_filter
group by cf.id_categori, contador.Fecha, contador.Hora) as contador_categorias
join filtering_categorizations ct on contador_categorias.id_categori = ct.id
order by Fecha, Hora, ct.id"

    @datos = ActiveRecord::Base.connection.exec_query(sql)


    if @datos.nil?
      puts 'No hay Datos'
    else
      puts 'Si hay datps'
    end

    registro = ""
    hora2 = ""
    keys = {}
    color = {}
    datosScript = Array.new
    total = 0
    hash = {}
    colorHash = {}
    count = 0
    @tx_filter = FilteringCategorization.all
    @tx_filter.each do |txfil|
      count+=3
      hash.store(txfil.id, txfil.name_categorization)
      colorr = Colors.obtenerColor(count)
      colorHash.store(txfil.id, colorr)
    end

    clores = colorHash


    @datos.each do |datos|
      idFil = datos['id']
      hora = datos['Hora']
      valores = datos['suma']

      comp = hash[idFil]
      label = hash[idFil].to_s.downcase.tr(" ", "_")

      if hora != hora2.to_s
        if registro.to_s.length > 0
          total = count
          datosScript.push(("{"+registro+"},").gsub("", "").html_safe).to_s
        end
        count = 0
        hora2 = hora
        registro = "time: '"+hora2+"'"
      end
      count += 1

      if total <= count
        keys.store(count, label)
        color.store(count, colorHash[idFil])
      end

      registro += ",#{label}: #{valores}"
    end

    if registro.to_s.length > 0
      datosScript.push(("{#{registro}}").gsub("", "").html_safe).to_s
    end

    keysScript = Array.new
    colorScript = Array.new
    n = 0
    keys.each do |k|
      nu = n+=1
      if nu < keys.length
        keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
      elsif nu = keys
        keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
      end
    end

    d = 0
    color.each do |c|
      nu = d+=1
      if nu < color.length
        colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
      elsif nu = color
        colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
      end
    end

    keysScriptD = Array.new
    colorScriptD = Array.new
    n = 0
    keys.each do |k|
      nu = n+=1
      if nu < keys.length
        keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
      elsif nu = keys
        keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
      end
    end

    d = 0
    color.each do |c|
      nu = d+=1
      if nu < color.length
        colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
      elsif nu = color
        colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
      end
    end

    @label = keysScript
    @color = colorScript

    @labelD = keysScriptD
    @colorD = colorScriptD
    @datosScript = datosScript

    respond_to do |f|
      f.html {render partial: 'dynamic'}
    end


  end

  def pies

    sql = "select ct.id , suma from
(select cf.id_categori , sum(cantidad) suma from
(select idFiltro, count(*) cantidad from tbitacora where IdTipoReg = 3 group by idFiltro) as contador
left join categori_filters cf on contador.IdFiltro = cf.id_filter
group by cf.id_categori) as contador_categorias
join filtering_categorizations ct on contador_categorias.id_categori = ct.id
order by ct.id"

    hash = {}
    chash = {}


    datosScript = Array.new
    @datos = ActiveRecord::Base.connection.exec_query(sql)

    fil_cat = FilteringCategorization.all

    contador = 0
    fil_cat.each do |fi_ca|
      contador+=1
      hash.store(fi_ca.id,fi_ca.name_categorization)
      chash.store(fi_ca.id,Colors.obtenerColor(contador))
    end


    @datos.each do |da|
      idCar = da['id']
      valor = da['suma'].to_s.downcase.tr(" ", "_")
      label = hash[idCar].to_s.downcase.tr(" ", "_")
      color = chash[idCar].to_s.downcase.tr(" ", "_")

      registro = "label: '#{label}', data: #{valor}, color: '#{color}'"

      if registro.to_s.length
        datosScript.push(("{#{registro} },").gsub(" ", " ").html_safe).to_s
      end

      #registro = "label: '#{label}', data: #{valor}"

    end

    @valores = datosScript

    respond_to do |f|
      f.html {render partial: 'pies'}
    end
  end
end