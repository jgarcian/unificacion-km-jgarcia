class TxFilterDetailsController < ApplicationController
  before_action :set_tx_filter_detail, only: [:show, :edit, :update, :destroy]

  # GET /tx_filter_details
  # GET /tx_filter_details.json
  def index
    @tx_filter_details = TxFilterDetail.all
  end

  # GET /tx_filter_details/1
  # GET /tx_filter_details/1.json
  def show
  end

  # GET /tx_filter_details/new
  def new
    @tx_filter_detail = TxFilterDetail.new
  end

  # GET /tx_filter_details/1/edit
  def edit
  end

  # POST /tx_filter_details
  # POST /tx_filter_details.json
  def create

    # @filtro = params[:filtro]
    # # @keyId = params[:keyId]
    # @cont = 0
    # @numCondi = 0
    # @crearCondicion = false
    #
    # @conector
    # @negado
    # @operador
    # @parentesis
    # @valor
    # @campoFormatoId
    # @posision
    # @length
    #
    # @filtro.each do | datoFiltro |
    #   @cont += 1
    #
    #   if ( @cont === 1 )
    #     @conector = datoFiltro
    #   elsif ( @cont === 2 )
    #     @negado = datoFiltro
    #   elsif ( @cont === 3 )
    #     @operador = datoFiltro
    #   elsif ( @cont === 4 )
    #     @parentesis = datoFiltro
    #   elsif ( @cont === 5 )
    #     @valor = datoFiltro
    #   elsif ( @cont === 6 )
    #     @campoFormatoId = datoFiltro
    #   elsif ( @cont === 7 )
    #     @posision = datoFiltro
    #   elsif ( @cont === 8 )
    #     @length = datoFiltro
    #     @cont = 0
    #     @crearCondicion = true
    #   end
    #
    #   if ( @crearCondicion  === true )
    #     @crearCondicion = false
    #     @numCondi += 1
    #     # @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params) marco error enviandolo desde el metodo new de session
    #     @tx_filter_detail = TxFilterDetail.new
    #     @tx_filter_detail.conditionnumber = @numCondi
    #     @tx_filter_detail.connector = @conector
    #     @tx_filter_detail.denied = @negado
    #     @tx_filter_detail.operator = @operador
    #     @tx_filter_detail.parenthesis = @parentesis
    #     @tx_filter_detail.value = @valor
    #     @tx_filter_detail.fieldFormat_id = @campoFormatoId
    #     @tx_filter_detail.position = @posision
    #     @tx_filter_detail.lenght = @length
    #     @tx_filter_detail.key_id = @keyId
    #     @tx_filter_detail.save
    #   end
    # end



    # respond_to do |format|
    #   if @tx_filter_detail.save
    #     format.html { redirect_to @tx_filter_detail, notice: 'Tx filter detail was successfully created.' }
    #     format.json { render :show, status: :created, location: @tx_filter_detail }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /tx_filter_details/1
  # PATCH/PUT /tx_filter_details/1.json
  def update
    respond_to do |format|
      if @tx_filter_detail.update(tx_filter_detail_params)
        format.html { redirect_to @tx_filter_detail, notice: 'Tx filter detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_filter_detail }
      else
        format.html { render :edit }
        format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_filter_details/1
  # DELETE /tx_filter_details/1.json
  def destroy

    @tx_filter_detail_delete = TxFilterDetail.where(:filter_id => nil).where(:key_id => current_user.id)

    @tx_filter_detail_delete.each do | condiciones |
      condiciones.destroy
    end

    # @tx_filter_detail.destroy
    respond_to do |format|
      # format.html { redirect_to tx_filter_details_url, notice: 'Tx filter detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filter_detail
      @tx_filter_detail = TxFilterDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filter_detail_params
      params.require(:tx_filter_detail).permit(:filter_id, :conditionnumber, :connector, :denied, :operator, :parenthesis, :value, :fieldFormat_id, :position, :lenght, :key_id)
    end
end
