class ComparasionsController < ApplicationController
  #Porcentual


  #before_filter :authenticate_user!, :except => [:dynamic, :index]
  #before_action :set_user_work_space, only: [:show, :edit, :update, :destroy]

  #before_filter :authenticate_user!, :except => [:dynamic]
  before_action :authenticate_user!


  respond_to :html, :js

  def index

  end

  # def create
  #   if params[:fecha1].present? || params[:fecha2].present?
  #     t1 = ""
  #     t2 = ""
  #     vacio = true;
  #     total = 0
  #     registro = ""
  #     datosJS = ""
  #     count = 0
  #     keys = {}
  #     color = {}
  #     datosScript = Array.new
  #
  #     #### Crear hash con los nombres de los filtros y sus ID
  #     hash = {'0' => 'dumi'}
  #     chash = {'0' => '#FF0000'}
  #     @sesion = PorcentualSession.where("inactive = 0 ")
  #     @sesion.each do |ses|
  #       @filter = PorcentualFilter.where("porcentual_session_id = ?", ses.id)
  #       @filter.each do |fil|
  #         @fil_id = fil.id.to_s
  #         @fil_name = fil.description.to_s
  #         @fil_color = fil.backgroundColor
  #         chash.store(@fil_id, @fil_color)
  #         hash.store(@fil_id, @fil_name)
  #       end
  #     end
  #
  #     @f1 = params[:fecha1].to_s.to_datetime
  #     @f2 = params[:fecha2].to_s.to_datetime
  #     puts @f1 =  @f1.strftime('%Y%m%d').to_s
  #     puts @f2 =  @f2.strftime('%Y%m%d').to_s
  #     @sess =   params[:session].to_s.rjust(4, '0');
  #     @tabla1 = 'TR'+ @f1.to_s + @sess.to_s
  #     @tabla2 = 'TR'+ @f2.to_s + @sess.to_s
  #
  #     ### Sacar el nombre de la base que se tiene configurada el aplicativo
  #     nameDatabase = Rails.configuration.database_configuration[Rails.env]["database"]
  #     @tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_schema='"+ nameDatabase +"' and table_name between '"+@tabla1+"' and '"+@tabla2+"' and table_name like '%"+@sess.to_s+"' order by tabla ASC")
  #
  #     ### Conjunto de tablas del intervalo de la fecha1 y fecha2
  #     @tablas.each do |tablas|
  #       @t_all = tablas.to_s.gsub('["','').gsub('"]','')
  #       @datos = ActiveRecord::Base.connection.exec_query("SELECT * from "+ @t_all)
  #       @fecha_tabla = @t_all.to_s.gsub('TR','').gsub(@sess,'').to_datetime.strftime('%Y-%m-%d').to_s
  #       @datos.each do |todo|
  #         vacio = false;
  #         idFil = todo[1].to_s
  #         t1 = todo[2].to_s
  #         if t1 != t2.to_s
  #           if registro.to_s.length > 0
  #             total = count
  #             datosScript.push(("{"+registro+"},").gsub("", "").html_safe).to_s
  #           end
  #           count = 0
  #           t2 = t1
  #           registro = "time :'"+@fecha_tabla+","+t2+"'"
  #         end
  #         label = hash[idFil].downcase.tr(" ", "_")
  #         if total <= count
  #           keys.store(count, label)
  #           color.store(count, chash[idFil])
  #         end
  #         val = todo[3].to_s
  #         count += 1
  #         registro += ","+label+":"+val
  #       end
  #       if registro.to_s.length > 0
  #         datosScript.push(("{"+registro+"}").gsub("", "").html_safe).to_s
  #       end
  #     end
  #     if registro.to_s.length > 0
  #       puts datosScript.push(("{"+registro+"}").gsub("", "").html_safe).to_s
  #     end
  #     keysScrip = Array.new
  #     colorScrip = Array.new
  #     n = 0
  #     keys.each do |yk|
  #       nu = n += 1
  #       if nu < keys.length
  #         puts keysScrip.push(("'"+yk[1]+"',").gsub!("", "").html_safe).to_s
  #       elsif nu = keys
  #         puts keysScrip.push(("'"+yk[1]+"' ").gsub!("", "").html_safe).to_s
  #       end
  #     end
  #     d = 0
  #     color.each do |ykc|
  #       nu = d += 1
  #       if nu < color.length
  #         puts  colorScrip.push(("'"+ykc[1]+"' ,").gsub("", "").html_safe).to_s
  #       elsif nu = color
  #         puts  colorScrip.push(("'"+ykc[1]+"'").gsub("", "").html_safe).to_s
  #       end
  #     end
  #
  #     @datosScript = datosScript
  #     @label = keysScrip
  #     @color = colorScrip
  #
  #     respond_to do |f|
  #       if (vacio)
  #         f.html { render :partial => 'empty' }
  #       else
  #         f.html { render :partial => 'dynamic' }
  #       end
  #     end
  #   end
  # end

  def dynamic

    @btn = true

    if (params[:fecha1].present? || params[:fecha2].present? || params[:btn] == "btn1")
      t1 = ""
      t2 = ""
      vacio = true;
      total = 0
      registro = ""
      datosJS = ""
      count = 0
      keys = {}
      color = {}
      datosScript = Array.new
      area = params[:area]

      #### Crear hash con los nombres de los filtros y sus ID
      hash = {'0' => 'dumi'}
      chash = {'0' => '#FF0000'}
      @sesion = PorcentualSession.where("inactive = 0 ")
      @sesion_id = params[:sess]
      @sesion.each do |ses|

        if (ses.id == params[:sess].to_i)
          @sessionName = ses.description
        end
        @filter = PorcentualFilter.where("porcentual_session_id = ?", ses.id)
        @filter.each do |fil|
          @fil_id = fil.id.to_s
          @fil_name = fil.description.to_s
          if fil.backgroundColor.nil?
            @fil_color = "#2EFE2E"
          else
            @fil_color = fil.backgroundColor
          end


          chash.store(@fil_id, @fil_color)
          hash.store(@fil_id, @fil_name)
        end
      end


      valor1 = chash
      valor2 = hash

      @fechaInicio = params[:fecha1]
      @fechaFin = params[:fecha2]

      @f1 = params[:fecha1].to_s.to_datetime.strftime('%Y%m%d').to_s
      @f2 = params[:fecha2].to_s.to_datetime.strftime('%Y%m%d').to_s
      @sess = params[:sess].to_s.rjust(4, '0');
      @tabla1 = 'TR' + @f1.to_s + @sess.to_s
      @tabla2 = 'TR' + @f2.to_s + @sess.to_s

      ### Sacar el nombre de la base que se tiene configurada el aplicativo
      nameDatabase = Rails.configuration.database_configuration[Rails.env]["database"]
      #@tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_schema='"+ nameDatabase +"' and table_name between '"+@tabla1+"' and '"+@tabla2+"' and table_name like '%"+@sess.to_s+"' order by tabla ASC")
      @tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_catalog='" + nameDatabase + "' and table_name between '" + @tabla1 + "' and '" + @tabla2 + "' and table_name like '%" + @sess.to_s + "' order by tabla ASC")

      ### Conjunto de tablas del intervalo de la fecha1 y fecha2
      @tablas.each do |tablas|

        #@t_all = tablas.to_s.gsub('["', '').gsub('"]', '')
        @t_all = tablas['tabla']
        @datos = ActiveRecord::Base.connection.exec_query("SELECT * from " + @t_all)
        @fecha_tabla = @t_all.to_s.gsub('TR', '').gsub(@sess, '').to_datetime.strftime('%Y/%m/%d').to_s

        @datos.each do |todo|
          vacio = false;
          idFil = todo['procentual_filter_id'].to_s
          t1 = todo['time']
          if t1 != t2.to_s
            if registro.to_s.length > 0
              total = count
              datosScript.push(("#{registro}").gsub("", "").html_safe).to_s
            end
            count = 0
            t2 = t1
            # registro = "time :'" + @fecha_tabla + "," + t2 + "'"
            registro = ("#{@fecha_tabla}" " " "#{t2}" )
          end
          # valor = idFil
          label = hash[idFil].to_s.downcase.tr(" ", "_")

          # va1 = "#{count} - #{label}"
          # va2 = "#{count} - #{chash[idFil]}"

          if total <= count
            keys.store(count, label)
            color.store(count, chash[idFil])
          end
          val = todo['value'].to_s
          count += 1
          registro += ",#{val}"
          # puts registro
        end

      end


      @name_table = @t_all

      if registro.to_s.length > 0
        datosScript.push(("#{registro}").gsub("", "").html_safe).to_s
      end
      keysScrip = [("Time,").gsub!("", "").html_safe]
      colorScrip = Array.new
      n = 0
      keys.each do |yk|
        nu = n += 1
        k = yk
        if nu < keys.length
          keysScrip.push(("" + yk[1]).gsub!("", "").html_safe).to_s
        elsif nu = keys
          keysScrip.push(("" + yk[1] + " ").gsub!("", "").html_safe).to_s
        end
      end
      d = 0
      color.each do |ykc|
        nu = d += 1
        c = ykc
        if nu < color.length
          colorScrip.push(("'" + ykc[1]).gsub("", "").html_safe).to_s
        elsif nu = color
          colorScrip.push(("'" + ykc[1] + "'").gsub("", "").html_safe).to_s
        end
      end

      @datosScript = datosScript
      @label = keysScrip
      @color = colorScrip

      @area = area
      if (@area == 'session1')
        @idChart = 'chart1'
        gon.area = @idChart
      elsif (@area == 'session2')
        @idChart = 'chart2'
        gon.area = @idChart
      end

      respond_to do |f|
        if (vacio)
          f.html {render :partial => 'empty'}
        else
          f.html {render :partial => 'dynamic'}
        end
      end


    elsif (params[:fecha1].present? || params[:fecha2].present? || params[:btn] == "btn2")
      t2 = ""
      vacio = true;
      total = 0
      registro = ""
      datosJS = ""
      count = 0
      keys = {}
      color = {}
      datosScript = Array.new
      area = params[:area]

      #### Crear hash con los nombres de los filtros y sus ID
      hash = {'0' => 'dumi'}
      chash = {'0' => '#FF0000'}
      @sesion = PorcentualSession.where("inactive = 0 ")
      @sesion_id = params[:sess]
      @sesion.each do |ses|
        @filter = PorcentualFilter.where("porcentual_session_id = ?", ses.id)
        @filter.each do |fil|
          @fil_id = fil.id.to_s
          @fil_name = fil.description.to_s
          @fil_color = fil.backgroundColor
          chash.store(@fil_id, @fil_color)
          hash.store(@fil_id, @fil_name)
        end
      end

      @f1 = params[:fecha1].to_s.to_datetime.strftime('%Y%m%d').to_s
      @f2 = params[:fecha2].to_s.to_datetime.strftime('%Y%m%d').to_s
      @sess = params[:sess].to_s.rjust(4, '0');
      @tabla1 = 'TR' + @f1.to_s + @sess.to_s
      @tabla2 = 'TR' + @f2.to_s + @sess.to_s

      ### Sacar el nombre de la base que se tiene configurada el aplicativo
      nameDatabase = Rails.configuration.database_configuration[Rails.env]["database"]
      @tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_schema='" + nameDatabase + "' and table_name between '" + @tabla1 + "' and '" + @tabla2 + "' and table_name like '%" + @sess.to_s + "' order by tabla ASC")

      ### Conjunto de tablas del intervalo de la fecha1 y fecha2
      @tablas.each do |tablas|
        @t_all = tablas.to_s.gsub('["', '').gsub('"]', '')
        @datos = ActiveRecord::Base.connection.exec_query("SELECT * from " + @t_all)
        @fecha_tabla = @t_all.to_s.gsub('TR', '').gsub(@sess, '').to_datetime.strftime('%Y-%m-%d').to_s
        @datos.each do |todo|
          vacio = false;
          idFil = todo['id'].to_s
          t1 = todo['time'].to_s
          if t1 != t2.to_s
            if registro.to_s.length > 0
              total = count
              datosScript.push(("#{registro}").gsub("", "").html_safe).to_s
            end
            count = 0
            t2 = t1
            # registro = "time :'" + @fecha_tabla + "," + t2 + "'"
            registro = "#{@fecha_tabla}"
          end
          label = hash[idFil].downcase.tr(" ", "_")
          if total <= count
            keys.store(count, label)
            color.store(count, chash[idFil])
          end
          val = todo['value'].to_s
          count += 1
          registro += "," + label + ":" + val
        end

      end

      if registro.to_s.length > 0
        # puts datosScript.push(("{" + registro + "}").gsub("", "").html_safe).to_s
        datosScript.push(("#{registro}").gsub("", "").html_safe).to_s
      end
      keysScrip = [("Time,").gsub!("", "").html_safe]
      colorScrip = Array.new
      n = 0
      keys.each do |yk|
        nu = n += 1
        if nu < keys.length
          # puts keysScrip.push(("'" + yk[1] + "',").gsub!("", "").html_safe).to_s
          keysScrip.push(("" + yk[1]).gsub!("", "").html_safe).to_s
        elsif nu = keys
          # puts keysScrip.push(("'" + yk[1] + "' ").gsub!("", "").html_safe).to_s
          keysScrip.push(("" + yk[1] + " ").gsub!("", "").html_safe).to_s
        end
      end
      d = 0
      color.each do |ykc|
        nu = d += 1
        if nu < color.length
          # puts colorScrip.push(("'" + ykc[1] + "' ,").gsub("", "").html_safe).to_s
          colorScrip.push(("'" + ykc[1]).gsub("", "").html_safe).to_s
        elsif nu = color
          # puts colorScrip.push(("'" + ykc[1] + "'").gsub("", "").html_safe).to_s
          colorScrip.push(("'" + ykc[1] + "'").gsub("", "").html_safe).to_s
        end
      end

      @datosScript = datosScript
      @label = keysScrip
      @color = colorScrip

      @area = area
      if (@area == 'session1')
        @idChart = 'chart1'
        gon.area = @idChart
      elsif (@area == 'session2')
        @idChart = 'chart2'
        gon.area = @idChart
      end

      respond_to do |f|
        if (vacio)
          f.html {render :partial => 'empty'}
        else
          f.html {render :partial => 'load'}
          f.html {render :partial => 'dynamic'}
        end
      end
    end

  end

  def load
    respond_to do |f|
      f.html {render :partial => 'load'}
    end
  end

  def empty
    respond_to do |f|
      @btn2 = true
      f.html {render :partial => 'empty'}
    end
  end


end