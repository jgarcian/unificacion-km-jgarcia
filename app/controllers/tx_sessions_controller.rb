class TxSessionsController < ApplicationController
  before_action :set_tx_session, only: [:show, :edit, :update, :destroy]

  # GET /tx_sessions
  # GET /tx_sessions.json
  def index
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    # Gon para Description
    desFiltro = TxFilters.all
    gon.desFiltros = desFiltro

    ## Gon para llenar selects Layout, Formato de ventana modal
    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

  end

  # GET /tx_sessions/1
  # GET /tx_sessions/1.json
  def show
  end

  # GET /tx_sessions/new
  def new
    @tx_session = TxSessions.new
    @edt = false;

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true



  end

  # GET /tx_sessions/1/edit
  def edit
    @edt = true;
  end

  # POST /tx_sessions
  # POST /tx_sessions.json
  def create
    ##### Vairables de AJAX #####
    # // Variables de la Sesión
    @idLay = params[:idLay]
    @idFormat = params[:idFormato]
    @desSession = params[:nameSession]

    # // Variables del Filtro
    @desFilter = params[:nameFilter]
    @typeFilter = params[:typeFilter]
    @background = params[:general]

    # // Detalle del filtro
    @detalleFiltro = params[:filtro]
    @idUser = params[:keyId]

    # @tx_session = TxSessions.new(tx_session_params)
    @tx_session = TxSessions.new
    @tx_session.Lay_id = @idLay
    # @tx_session.Format_id = @idFormat
    @tx_session.Description = @desSession
    @tx_session.Format_id= @idFormat
    @tx_session.Inactive = 0
    @tx_session.Update = 0
    @tx_session.save

    respond_to do |format|
      if @tx_session.save

        @tx_session_filter = TxFilters.new
        @tx_session_filter.Session_id = @tx_session.id
        @tx_session_filter.Description = @desFilter
        @tx_session_filter.Order_number = @typeFilter
        @tx_session_filter.save

        #// Detalle del Filtro
        @cont = 0
        @numCondi = 0
        @crearCondicion = false

        @conector
        @negado
        @operador
        @parentesis
        @valor
        @campoFormatoId
        @posision
        @length

        @detalleFiltro.each do | datoFiltro |
          @cont += 1

          if ( @cont === 1 )
            @conector = datoFiltro
          elsif ( @cont === 2 )
            @negado = datoFiltro
          elsif ( @cont === 3 )
            @operador = datoFiltro
          elsif ( @cont === 4 )
            @parentesis = datoFiltro
          elsif ( @cont === 5 )
            @valor = datoFiltro
          elsif ( @cont === 6 )
            @campoFormatoId = datoFiltro
          elsif ( @cont === 7 )
            @posision = datoFiltro
          elsif ( @cont === 8 )
            @length = datoFiltro
            @cont = 0
            @crearCondicion = true
          end

          if ( @crearCondicion  === true )
            @crearCondicion = false
            @numCondi += 1
            # @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params) marco error enviandolo desde el metodo new de session
            @tx_filter_detail = TxFilterDetail.new
            @tx_filter_detail.filter_id = @tx_session_filter.id
            @tx_filter_detail.conditionnumber = @numCondi
            @tx_filter_detail.connector = @conector
            @tx_filter_detail.denied = @negado
            @tx_filter_detail.operator = @operador
            @tx_filter_detail.parenthesis = @parentesis
            @tx_filter_detail.value = @valor
            @tx_filter_detail.fieldFormat_id = @campoFormatoId
            @tx_filter_detail.position = @posision
            @tx_filter_detail.lenght = @length
            @tx_filter_detail.key_id = @idUser
            @tx_filter_detail.save
          end
        end

        # format.html { redirect_to action: :index, notice: 'Tx session was successfully created.' }
        format.html { redirect_to action: :index, notice: 'Tx session was successfully created.' }
        # @tx_session_filters_details = TxFilterDetail.where(:filter_id => nil).where(:key_id => current_user.id)
        # @tx_session_filters_details.each do | detalle |
        #   @cont += 1
        #   detalle.filter_id = @tx_session_filter.id
        #   detalle.conditionnumber = @cont
        #   detalle.key_id = nil
        #   detalle.save
        # end

        # format.html { redirect_to @tx_session, notice: 'Tx session was successfully created.' }
        # format.json { render :show, status: :created, location: @tx_session }
        format.json { render :index, location: @tx_session }
      else
        format.html { render :new }
        format.json { render json: @tx_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tx_sessions/1
  # PATCH/PUT /tx_sessions/1.json
  def update
    respond_to do |format|
      if @tx_session.update(tx_session_params)
        format.html { redirect_to action: :index, notice: 'Tx session was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_session }
      else
        format.html { render :edit }
        format.json { render json: @tx_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_sessions/1
  # DELETE /tx_sessions/1.json
  def destroy

    @tx_session_filters = TxFilters.where(:Session_id => @tx_session.id)

    @tx_session_filters.each do |filter|

      @tx_session_filters_details = TxFilterDetail.where(:filter_id => filter.id)

      @tx_session_filters_details.each do | filterDeatil |
        filterDeatil.destroy
      end

      filter.destroy
    end

    @tx_session.destroy
    respond_to do |format|
      # format.html { redirect_to tx_sessions_url, notice: 'Tx session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_session
      @tx_session = TxSessions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_session_params
      params.require(:tx_sessions).permit(:Id, :Lay_id, :Format_id, :Description, :Sessioncolor, :Interval, :Inactive)
    end
end
